![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Documentation pour INTjs :
La doc relative à INTjs pour devenir des super développeurs INT
https://docs.intchain.io/intjs/

## Site internet
Ce repo génère le site internet que vous trouverez ici : 
https://cryptodevkheys.gitlab.io
Vous êtes libres de créer vos propres articles, c'est une sorte de mémoire de groupe. L'idée
c'est qu'on garde une trace de nos découvertes, pour que ça puisse aider les autres.


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
