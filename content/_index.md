## Un p'tit mot pour les kheys

Bon j'ai eu l'idée de faire ce petit site. Je me suis dit que ça nous permettrait d'avoir une trace de notre progression sous la main.
Le but, c'est que chacun puisse écrire son petit post au fur et à mesure de ses découvertes, pour aider tout le monde !
On est beaucoup issus de la communauté INT, et un des objectifs est de se former à [INTjs](https://docs.intchain.io/intjs/) et par la suite d'être capables d'envisager du dev sur la blockchain INT.

### Le principe est simple
Derrière ce site, il y a un repo [GitLab](https://gitlab.com). Créez votre compte si vous n'en avez pas déjà un, et je vous ajouterai au groupe GitLab. L'avantage des groupes c'est qu'on a accès à tous les projets du groupe, donc ça nous permettra d'essayer de mettre en place des trucs, de les bosser ensemble, de les améliorer, sans se sentir enfermés dans *un seul projet* dans lequel il faudrait organiser les dossiers, machin truc.

Dans le groupe, il y a un seul repo pour l'instant : `cryptodevkheys.gitlab.io`.
Si vous voulez créer un article (ou un post, comme vous le sentez), il suffit de créer un fichier dans le dossier `Content/Posts`.

Pour le nom du fichier : `aaaa-mm-jj-titre.md`

Le contenu du fichier, c'est du Markdown classique. Pour qu'il soit automatiquement ajouté dans le site, vous devez simplement inclure au début du fichier cet en-tête :
```
---
title: Votre titre
subtitle: Un sous-titre si vous voulez
date: 2017-03-27 (la date dans ce format-là)
tags: ["example", "bigimg"] 
(dans les tags pensez à mettre votre pseudo, comme ça on s'y retrouvera)
---
```

Et puis en-dessous, vous mettez votre article au format Markdown. J'ai laissé les articles-exemples, comme ça si vous voulez vous familiariser avec la syntaxe vous pouvez vous balader dans le repo et regarder à quoi les fichiers *.md ressemblent.

L'avantage, c'est que rien ne vous empêche d'écrire vos articles sur votre ordi, téléphone, machine à écrire, et de balancer le fichier dans le dossier `Content/Posts` quand vous voulez le mettre sur le site !

Bon ben voilà, j'ai posé ça là, on verra ce qu'on en fait !